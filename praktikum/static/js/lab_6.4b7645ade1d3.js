// chat box
var head = document.getElementById ('arrow');
var body = document.getElementsByClassName ('chat-body');
var text = document.getElementById ('my-text');
var textField = document.getElementsByClassName ('msg-insert');
var send = true;

$(text).keypress(function(e){
  if(e.keyCode === 13){
    if (send){
      $(textField).append('<p class = "msg-send">'+ text.value + '</p>')
      send = false;
    }
    else{
      $(textField).append('<p class = "msg-receive">'+ text.value + '</p>')
      send = true;
    }
    text.value = "";
  }

});

$(head).click(function(){
  $(body).toggle();
})
// END

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = '';
    erase = true;
  } else if (x === 'eval') {
    print.value = Math.round(evil(print.value) * 10000) / 10000;
    erase = true;
  } else if (x === 'sin'){
    print.value = Math.sin(print.value);
  } else if (x === 'log'){
  print.value = Math.log(print.value) / Math.log(10);
  } else if (x === 'tan'){
    print.value = Math.tan(print.value);
  }else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

// Theme
// Select2
function changeTheme(newTheme){
    $('body').css({"backgroundColor": newTheme['bcgColor']});
    $('.text-center').css({"color": newTheme['fontColor']});
}

if (localStorage.getItem('themes') === null){
  localStorage.setItem('themes','[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":1,"text":"Pink","bcgColor":"#F5668F","fontColor":"#FAFAFA"},{"id":2,"text":"Purple","bcgColor":"#3","fontColor":"#FAFAFA"},{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#FAFAFA"},{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#FAFAFA"},{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#FAFAFA"},{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#FAFAFA"},{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#FAFAFA"},{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#FAFAFA"},{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]');
}

var themes = JSON.parse(localStorage.getItem('themes'));

if (localStorage.getItem('selectedTheme') === null) {
  localStorage.setItem('selectedTheme', JSON.stringify(themes[3]));
}

var theme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(theme);

$(document).ready(function() {
    $('.my-select').select2({'data' : themes}).val(theme['id']).change();
    $('.apply-button').on('click', function(){
        theme = themes[$('.my-select').val()];
        changeTheme(theme);
        localStorage.setItem('selectedTheme',JSON.stringify(theme));
    })
});