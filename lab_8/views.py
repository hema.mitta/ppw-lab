from django.shortcuts import render

def index(request):
    response = {}
    response['author'] = "Hema Mitta"
    return render(request, 'lab_8/lab_8.html', response)