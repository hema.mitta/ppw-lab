from django.shortcuts import render

def index(request):
    response = {}
    response['author'] = "Hema Mitta"
    return render(request, 'lab_6/lab_6.html', response)